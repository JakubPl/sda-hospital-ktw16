import pl.sda.model.Disease;
import pl.sda.model.Patient;
import pl.sda.service.HospitalQueueService;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Witaj uzytkowniku!");

        HospitalQueueService hospitalQueueService = new HospitalQueueService();

        boolean shouldExit = false;
        Scanner scanner = new Scanner(System.in);
        while (!shouldExit) {
            printMenu();
            String line = scanner.nextLine();
            if(line.equals("1")) {
                System.out.println("Podaj imie");
                String name = scanner.nextLine();
                System.out.println("Podaj nazwisko");
                String surname = scanner.nextLine();
                System.out.println("Podaj jak bardzo zly");
                String howAngry = scanner.nextLine();
                System.out.println("Podaj chorobe");
                String illness = scanner.nextLine();
                Integer howAngryInt = Integer.valueOf(howAngry);

                Patient patient = new Patient(name, surname, howAngryInt, Disease.valueOf(illness.toUpperCase()));
                hospitalQueueService.addPatient(patient);
                //tutaj pobieranie danych pacjenta
                //i dodawania go do kolejki
            } else if(line.equals("2")) {
                Patient nextPatient = hospitalQueueService.next();
                System.out.printf("%s %s%n", nextPatient.getName(), nextPatient.getSuname());
                //tutaj pobranie nastepnego pacjenta
            } else if (line.equals("3")) {
                Patient nextPatient = hospitalQueueService.peek();
                System.out.printf("%s %s%n", nextPatient.getName(), nextPatient.getSuname());
                //podejrzenie nastepnego pacjenta
            } else if (line.equals("4")) {
                shouldExit = true;
            }
        }
    }

    private static void printMenu() {
        System.out.println("1. Nowy pacjent");
        System.out.println("2. Nastepny pacjent");
        System.out.println("3. Podejrzyj pacjenta");
        System.out.println("4. Zakoncz");
    }
}
