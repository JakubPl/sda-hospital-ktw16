package pl.sda.service;

import pl.sda.model.Patient;

import java.util.PriorityQueue;

public class HospitalQueueService {
    private PriorityQueue<Patient> hospitalQueue = new PriorityQueue<>(new PatientComparator());

    public void addPatient(Patient patient) {
        hospitalQueue.add(patient);
        //dodoawanie pacjenta
    }

    public Patient next() {
        return hospitalQueue.poll();
        //zwracanie kolejnego pacjenta
    }

    public Patient peek() {
        return hospitalQueue.peek();
        //podgladanie jaki jest nastepn pacjent
    }
}
