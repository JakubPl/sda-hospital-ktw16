package pl.sda.service;

import pl.sda.model.Disease;
import pl.sda.model.Patient;

import java.util.Comparator;

public class PatientComparator implements Comparator<Patient> {

    public static final String KOWALSKI = "Kowalski";

    @Override
    public int compare(Patient o1, Patient o2) {
        boolean isO1Kowalski = o1.getSuname().equals(KOWALSKI);
        boolean isO2Kowalski = o2.getSuname().equals(KOWALSKI);
        if (isO1Kowalski && !isO2Kowalski) {
            return -1;
        } else if (!isO1Kowalski && isO2Kowalski) {
            return 1;
        } else {
            boolean isO1Serious = Disease.SOMETHING_SERIOUS.equals(o1.getDisease());
            boolean isO2Serious = Disease.SOMETHING_SERIOUS.equals(o2.getDisease());
            if (isO1Serious && !isO2Serious) {
                return -1;
            } else if (!isO1Serious && isO2Serious) {
                return 1;
            } else {
                int o1Rank = o1.getHowAngry() * o1.getDisease().getInfectiousness();
                int o2Rank = o2.getHowAngry() * o2.getDisease().getInfectiousness();
                return o2Rank - o1Rank;
            }
        }
    }
}
