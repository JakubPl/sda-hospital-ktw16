package pl.sda.model;

public enum Disease {
    FLU(1), COLD(2), SOMETHING_SERIOUS(3), DIARREA(4);
    int infectiousness;

    Disease(int infectiousness) {
        this.infectiousness = infectiousness;
    }

    public int getInfectiousness() {
        return infectiousness;
    }
}
