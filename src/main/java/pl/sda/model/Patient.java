package pl.sda.model;

public class Patient {
    private String name;
    private String suname;
    private int howAngry;
    private Disease disease;

    public Patient(String name, String suname, int howAngry, Disease disease) {
        this.name = name;
        this.suname = suname;
        this.howAngry = howAngry;
        this.disease = disease;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSuname() {
        return suname;
    }

    public void setSuname(String suname) {
        this.suname = suname;
    }

    public int getHowAngry() {
        return howAngry;
    }

    public void setHowAngry(int howAngry) {
        this.howAngry = howAngry;
    }

    public Disease getDisease() {
        return disease;
    }

    public void setDisease(Disease disease) {
        this.disease = disease;
    }
}
